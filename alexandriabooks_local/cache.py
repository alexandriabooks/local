import logging
import typing
import os
import os.path
import time
import threading

from alexandriabooks_core import config
from alexandriabooks_core.services import CacheService

logger = logging.getLogger(__name__)
__file_locks__: typing.Dict[str, typing.Dict[str, typing.Any]] = {}
__file_locks_map_lock__ = threading.Lock()


def _get_cache_file_lock(key):
    with __file_locks_map_lock__:
        file_lock = __file_locks__.get(key)
        if file_lock is None:
            file_lock = {"key": key, "lock": threading.RLock(), "refs": 1}
            __file_locks__[key] = file_lock
        else:
            file_lock["refs"] += 1
        return file_lock


def _release_cache_file_lock(key):
    with __file_locks_map_lock__:
        file_lock = __file_locks__.get(key)
        if file_lock is not None:
            file_lock["refs"] -= 1
            if file_lock["refs"] == 0:
                del __file_locks__[key]


class CacheFileWrapper:
    def __init__(self, filename, key):
        super().__init__()
        self.__filename = filename
        self.__key = key
        self.__lock = _get_cache_file_lock(self.__key)["lock"]
        self.__handle = None

    def __get_handle__(self):
        if self.__handle is None:
            self.__handle = open(self.__filename, "rb")
        return self.__handle

    def __getattr__(self, name):
        if self.__handle is None:
            _release_cache_file_lock(self.__key)
        return getattr(self.__get_handle__(), name)

    def __enter__(self):
        self.__lock.acquire()
        self.__handle = open(self.__filename, "rb")
        return self.__handle

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__handle.flush()
        self.__handle.close()
        self.__lock.release()
        _release_cache_file_lock(self.__key)


class FileBasedCacheService(CacheService):
    def __init__(self, **kwargs):
        self._base = os.path.abspath(config.get("cache.directory", "cache"))

    def __str__(self):
        return f"Local Filesystem Cache: {self._base}"

    def _get_cache_path(self, key):
        return self._base + "/" + key.replace(":", "_")

    async def get_stream(self, key, value_func, max_age=None):
        try:
            cache_file_lock = _get_cache_file_lock(key)
            with cache_file_lock["lock"]:
                cache_path = self._get_cache_path(key)
                cache_entry = True
                if os.path.exists(cache_path):
                    mtime = os.path.getmtime(cache_path)
                    cur_time = time.time()
                    file_size = os.path.getsize(cache_path)
                    if ((max_age is None) or (mtime + max_age > cur_time)) and (
                        file_size > 0
                    ):
                        cache_entry = False
                        # logger.debug("Cache hit %s", key)
                    else:
                        os.remove(cache_path)

                if cache_entry:
                    if not os.path.exists(os.path.dirname(cache_path)):
                        os.makedirs(os.path.dirname(cache_path))
                    success = False
                    try:
                        logger.debug("Cache miss %s", key)
                        with open(cache_path, "wb") as out_fp:
                            success = await value_func(out_fp)
                    except Exception as exp:
                        os.remove(cache_path)
                        logging.error(
                            "Exception encountered during cache for key %s",
                            key,
                            exc_info=exp,
                        )
                        raise
                    if not success:
                        os.remove(cache_path)
                        raise Exception(f"Could not populate cache: {key}")

                return CacheFileWrapper(cache_path, key)
        finally:
            _release_cache_file_lock(key)

    async def cache_expired(self, key, max_age=None):
        cache_path = self._get_cache_path(key)
        if os.path.exists(cache_path):
            if max_age is None:
                return False
            file_stat = os.stat(cache_path)
            if (file_stat.st_mtime > (time.time() - max_age)) and (
                file_stat.st_size > 0
            ):
                return False
        return True
