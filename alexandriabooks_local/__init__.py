import logging
import typing
import os.path

from alexandriabooks_core import config

from .cache import FileBasedCacheService
from .sourceprovider import LocalBookSourceProviderService
from .cli import setup_cli

logger = logging.getLogger(__name__)

ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {
    "CacheService": [FileBasedCacheService],
    "CliCommand": [setup_cli]
}

__local_directory__ = config.get("local.directory")
if __local_directory__:
    if os.path.isdir(__local_directory__):
        ALEXANDRIA_PLUGINS["BookSourceProviderService"] = [LocalBookSourceProviderService]
        ALEXANDRIA_PLUGINS["BackgroundService"] = [LocalBookSourceProviderService]
    else:
        logger.warning(
            "Cannot find directory: %s, will not index books.",
            __local_directory__
        )
else:
    logger.debug("No local directory specified, won't load local directory source.")
