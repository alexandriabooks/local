import logging
import typing
import asyncio
import os.path
import concurrent.futures
from alexandriabooks_core import model, config
from alexandriabooks_core.services import (
    ServiceRegistry,
    BookSourceProviderService,
    BackgroundService
)
from alexandriabooks_core.accessors import LocalBookAccessor

logger = logging.getLogger(__name__)


class LocalBookSource(model.BookSource):
    def __init__(
        self,
        full_path: str,
        source_metadata: model.SourceProvidedBookMetadata
    ):
        super().__init__()
        self.full_path = full_path
        self.metadata = source_metadata

    def get_metadata(self) -> model.SourceProvidedBookMetadata:
        """Returns metadata for this book."""
        return self.metadata

    async def get_accessor(self) -> model.FileAccessor:
        """Returns a file accessor for this book source."""
        return LocalBookAccessor(self.full_path)


class BaseLocalBookSourceProviderService(BookSourceProviderService, BackgroundService):

    def __init__(self, **kwargs):
        self.service_registry: ServiceRegistry = kwargs["service_registry"]
        self.directory = config.get("local.directory")
        self.prefix = config.get("local.prefix", "local")
        default_refresh_time = 60 * 60
        self.refresh_time = config.get("local.refreshtime", default_refresh_time)
        self._refresh_task: typing.Optional[asyncio.Future] = None

    def __str__(self):
        return f"Local Book Source Provider: {self.directory}"

    async def initialize(self):
        """Initialize the service."""

    async def start(self):
        """Start the service."""
        logger.info("Starting refresh task...")
        self._refresh_task = asyncio.ensure_future(self._refresh_thread())

    async def stop(self):
        """Stop the service."""
        if self._refresh_task is not None:
            self._refresh_task.cancel()

    async def refresh_source_provider(self):
        pass

    @staticmethod
    def _generate_checksums(path, metadata):
        import hashlib

        hash_md5 = hashlib.md5()
        hash_sha1 = hashlib.sha1()
        hash_sha256 = hashlib.sha256()
        with open(path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
                hash_sha1.update(chunk)
                hash_sha256.update(chunk)
        metadata["md5"] = hash_md5.hexdigest()
        metadata["sha1"] = hash_sha1.hexdigest()
        metadata["sha256"] = hash_sha256.hexdigest()
        return True

    async def _parse_book(self, path, metadata):
        if not os.path.isfile(path):
            return

        title = os.path.basename(path)
        pos = title.rfind(".")
        if pos > 0:
            title = title[:pos]
        metadata["source_key"] = (
            self.prefix + ":" + path[len(self.directory):].strip("\\/")
        )
        metadata["title"] = title.replace("_", " ")

        file_stat = os.stat(path)

        metadata["size"] = file_stat.st_size
        metadata["acquisition_date"] = min(
            file_stat.st_ctime,
            file_stat.st_mtime,
            file_stat.st_atime
        )

        accessor = LocalBookAccessor(path)

        parsed = False
        try:
            if await self.service_registry.can_parse(accessor):
                parsed_metadata = await self.service_registry.parse_book(accessor)
                if parsed_metadata:
                    parsed = True
                    metadata.update(parsed_metadata)
        except Exception as exp:
            logger.error("Could not parse %s - %s", path, exp, exc_info=exp)

        if parsed:
            if "source_type" not in metadata:
                logger.warning(f"No source type provided for {path}")
            else:
                # LocalBookSourceProviderService._generate_checksums(path, metadata)
                logger.debug(
                    f"Found book {path}, type: {metadata['source_type'].name}"
                )
                await self.service_registry.add_book_source(
                    model.SourceProvidedBookMetadata(**metadata)
                )

    async def _scan_directory(self):
        event_loop: asyncio.BaseEventLoop = asyncio.get_running_loop()

        def walk_directory():
            parsing_futures = []
            for dir_name, subdir_list, file_list in os.walk(self.directory):
                for file_name in file_list:
                    full_path = os.path.join(dir_name, file_name)

                    coro = self._parse_book(full_path, {"local_file": full_path})
                    parsing_futures.append(asyncio.run_coroutine_threadsafe(
                        coro,
                        event_loop
                    ))

            if parsing_futures:
                concurrent.futures.wait(
                    parsing_futures,
                    return_when=concurrent.futures.ALL_COMPLETED
                )

        await event_loop.run_in_executor(
            None,
            walk_directory
        )

    async def _refresh_thread(self):

        try:
            logger.info(f"Running directory scans {self.directory}")
            await self._scan_directory()
        except Exception as e:
            logger.error(f"Could not refresh {self.directory}", exc_info=e)

        while True:
            try:
                await asyncio.sleep(self.refresh_time)
                logger.info(f"Running directory scans {self.directory}")
                await self._scan_directory()
            except Exception as e:
                logger.error(f"Could not refresh {self.directory}", exc_info=e)

    def _find_path_in_sandbox(self, rel_path: str):
        full_path = os.path.abspath(os.path.join(self.directory, rel_path))
        if len(self.directory) > len(full_path):
            return None

        common = os.path.commonpath([self.directory, full_path])

        if not common.startswith(self.directory):
            return None

        return full_path

    def get_prefix(self) -> str:
        """Returns the source prefix for this source."""
        return self.prefix

    async def get_book_source(
        self,
        source_metadata: model.SourceProvidedBookMetadata
    ) -> typing.Optional[model.BookSource]:
        full_path = self._find_path_in_sandbox(
            source_metadata.source_key[len(self.prefix) + 1:]
        )
        if not full_path:
            return None
        return LocalBookSource(full_path, source_metadata)

    async def get_book_source_by_key(
        self,
        source_key: str
    ) -> typing.Optional[model.BookSource]:
        source_metadata = await self.service_registry.get_book_source_metadata_by_key(
            source_key
        )
        if source_metadata:
            return await self.get_book_source(source_metadata)
        return None


LocalBookSourceProviderService: typing.Type[BaseLocalBookSourceProviderService] = \
    BaseLocalBookSourceProviderService

try:
    import inotify.adapters
    import inotify.constants

    class INotifyLocalBookSourceProviderService(BaseLocalBookSourceProviderService):

        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.inotify_watcher: typing.Optional[inotify.adapters.InotifyTree] = None
            default_refresh_time = 1
            self.refresh_time = config.get("local.refreshtime", default_refresh_time)

        async def start(self):
            """Start the service."""
            logger.info("Configuring inotify...")
            self.inotify_watcher = inotify.adapters.InotifyTree(
                self.directory,
                mask=(
                    inotify.constants.IN_MODIFY |
                    inotify.constants.IN_CREATE |
                    inotify.constants.IN_DELETE |
                    inotify.constants.IN_MOVE
                )
            )
            await super().start()

        async def stop(self):
            """Stop the service."""
            await super().stop()
            del self.inotify_watcher.inotify
            self.inotify_watcher = None

        async def _refresh_thread(self):
            try:
                logger.info(f"Running directory scans {self.directory}")
                await self._scan_directory()
            except Exception as e:
                logger.error(f"Could not refresh {self.directory}", exc_info=e)

            while True:
                await asyncio.sleep(self.refresh_time)
                futures = []
                for _, type_names, path, filename in self.inotify_watcher.event_gen(
                    yield_nones=False,
                    timeout_s=1
                ):
                    logger.debug(f"Got inotify event: {type_names} {path} {filename}")
                    if 'IN_ISDIR' in type_names:
                        continue
                    full_path = os.path.join(path, filename)
                    futures.append(asyncio.ensure_future(
                        self._parse_book(full_path, {"local_file": full_path})
                    ))
                if futures:
                    await asyncio.gather(*futures)

    if config.get_bool("local.inotify", True):
        LocalBookSourceProviderService = INotifyLocalBookSourceProviderService
except ImportError:
    pass
