import logging
import os.path
import asyncio
import click
from alexandriabooks_core.services import ServiceRegistry
from alexandriabooks_core.accessors import LocalBookAccessor

logger = logging.getLogger(__name__)


async def _async_parse_file(filename):
    service_registry = ServiceRegistry()
    await service_registry.initialize(load_plugins=["BookParserService"])
    logger.info("Initialized %s", service_registry)
    metadata = {}

    title = os.path.basename(filename)
    pos = title.rfind(".")
    if pos > 0:
        title = title[:pos]
    metadata["source_key"] = (
        "local:" + filename.strip("\\/")
    )
    metadata["title"] = title
    metadata["size"] = os.stat(filename).st_size

    accessor = LocalBookAccessor(filename)

    parsed = False
    try:
        if await service_registry.can_parse(accessor):
            parsed_metadata = await service_registry.parse_book(accessor)
            if parsed_metadata:
                parsed = True
                metadata.update(parsed_metadata)
    except Exception as exp:
        logger.error("Could not parse %s - %s", filename, exp, exc_info=exp)

    if parsed:
        print("Metadata:")
        for key, value in metadata.items():
            print(f"  {key} => {value}")
    else:
        logger.error("Not parsed...")


def setup_cli(cli_root):
    @cli_root.command()
    @click.option("--filename", "-f", required=True)
    def parse_file(filename):
        """Parses a passed file, and prints its detected metadata."""
        loop = asyncio.get_event_loop()
        loop.run_until_complete(_async_parse_file(filename))
